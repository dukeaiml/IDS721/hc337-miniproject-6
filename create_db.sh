#!/bin/bash

aws dynamodb create-table \
    --table-name miniproject-5 \
    --attribute-definitions \
        AttributeName=author,AttributeType=S \
        AttributeName=text,AttributeType=S \
    --key-schema \
        AttributeName=author,KeyType=HASH \
        AttributeName=text,KeyType=RANGE \
    --provisioned-throughput \
        ReadCapacityUnits=5,WriteCapacityUnits=5 \
    --table-class STANDARD
