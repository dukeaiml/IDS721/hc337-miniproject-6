use aws_config::{load_defaults, BehaviorVersion};
use aws_sdk_dynamodb::Client;
use lambda_http::{run, service_fn, Body, Error, Request, Response};
use rand::seq::SliceRandom;
use rand::thread_rng;
use serde::{Deserialize, Serialize};
use serde_dynamo::aws_sdk_dynamodb_1::from_items;
use tap::prelude::*;
use tracing::{debug, error, info};

#[tokio::main]
async fn main() -> Result<(), Error> {
    tracing_subscriber::fmt()
        .with_max_level(tracing::Level::INFO)
        // disable printing the name of the module in every log line.
        .with_target(false)
        // disabling time is handy because CloudWatch will add the ingestion time.
        .without_time()
        .init();

    let config = load_defaults(BehaviorVersion::latest()).await;
    let client = Client::new(&config);
    let client_ref = &client;
    let func = service_fn(move |req| async move { function_handler(req, client_ref).await });
    run(func).await?;
    Ok(())
}

#[derive(Serialize)]
struct LambdaResponse {
    message: String,
}

#[derive(Debug, Clone, Serialize, Deserialize)]
struct Quote {
    text: String,
    author: String,
}

async fn get_random_quote_from_dynamodb(table_name: &str, client: &Client) -> Option<Quote> {
    info!("Getting quotes from table {}", table_name);
    let req = client.scan().table_name(table_name).limit(32);
    let res = req
        .send()
        .await
        .tap_err(|e| error!("Error getting quotes from DynamoDB: {:?}", e))
        .ok()?;
    debug!("Received response");
    let items = res
        .items
        .tap_none(|| error!("Didn't find any quotes in the database"))?;
    debug!("Parsing response into quotes");
    let quotes: Vec<Quote> = from_items(items)
        .tap_err(|e| error!("Error while parsing quotes from database: {:?}", e))
        .ok()?;
    let quote = quotes.choose(&mut thread_rng())?;
    Some(quote.clone())
}

async fn function_handler(_req: Request, client: &Client) -> Result<Response<Body>, Error> {
    let quote = get_random_quote_from_dynamodb("miniproject-5", client)
        .await
        .ok_or_else(|| Error::from("Error accessing quote database"))?;
    let quote_str = format!("'{}' - {}", quote.text, quote.author);
    let resp = Response::builder()
        .status(200)
        .header("content-type", "text/html")
        .body(quote_str.into())
        .map_err(Box::new)?;
    Ok(resp)
}
